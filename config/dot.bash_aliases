#! /usr/bin/env bash

################################################################################
# General configuration for the shell - does not depend on Thingy to run
# but does initialise Thingy if it is found.
#
# Copyright (C) 2017 John Skilleter
################################################################################

# Key bindings

bind '"\e[A": history-search-backward'
bind '"\e[B": history-search-forward'

# Basic aliases

alias dir='ls -lh'
alias dirt='ls -rlth'
alias dira='ls -lah'
alias dirta='ls -ralth'

alias ls='ls --group-directories-first --color=auto'

alias more=less

alias ..='cd ..'
alias ...='cd ../..'
alias ....='cd ../../..'
alias .....='cd ../../../..'

shopt -s autocd

# Configuration variables

export TERM=xterm-256color
export PAGER=less

# Customise 'less' output

export LESS='  -j.5 --RAW-CONTROL-CHARS --shift=32 --status-column --prompt %T\:\ %f\ lines\:\ %lt-%lb\ col\:\ %c\ \[%bb/%B\]'

# Pipe less input through the source highlighter if it is available

if [[ -f /usr/share/source-highlight/src-hilite-lesspipe.sh ]]
then
   export LESSOPEN="| /usr/share/source-highlight/src-hilite-lesspipe.sh %s"
fi

# Save all shell command history
# for setting history length see HISTSIZE and HISTFILESIZE in bash(1)

HISTSIZE=5000
HISTFILESIZE=20000
HISTCONTROL=ignoredups

# append to the history file, don't overwrite it

shopt -s histappend

# Git aliases

# TODO: replace gits with git st when it is up to the job

#alias gits='git st'
alias gits='git status'
alias gcd='git cmp develop'
alias gcm='git cmp master'
alias grh='git reset --hard HEAD'
alias gg='ggrep'

# Ignore deleted files in the git cmp command

export GIT_IGNORE_DELETED=1

# Re-read the .dircolors file

function update_dircolors()
{
   ~/thingy/init-dircolors
   if test -r ~/.dircolors
   then
      eval "$(dircolors -b ~/.dircolors)"
   else
      eval "$(dircolors -b)"
   fi
}

# Enable colour highlighting in grep output

alias grep='grep --color=auto --exclude-dir=.git'
alias egrep='egrep --color=auto --exclude-dir=.git'
alias fgrep='fgrep --color=auto --exclude-dir=.git'
alias rgrep='rgrep --color=auto --exclude-dir=.git'

# Set up go

if [[ -z ${GOPATH:-} ]]
then
   export GOPATH=~/go
fi

mkdir -p $GOPATH

# cd to the top level directory of the current git working tree
# optionally stopping N directories down.

function cdg()
{
   local new_dir

   new_dir=$(git wt -pr ${1:-})

   if [[ -n "$new_dir" ]]
   then
      cd "$new_dir"
   fi
}

# Run the bash completion module (not run automatically for some reason)
# This code is taken from /etc/bash.bashrc where it is disabled by default

if ! shopt -oq posix
then
   if [[ -f /usr/share/bash-completion/bash_completion ]]
   then
      . /usr/share/bash-completion/bash_completion
   elif [[ -f /etc/bash_completion ]]
   then
      . /etc/bash_completion
   else
      echo >&2 "Warning - No shell auto-completion package installed"
   fi
fi

# Enable extended extended pattern matching in globbing (but _not_, apparently, in the 'find' utility!)

shopt -s extglob

# Add Thingy to the path & Python path and set the shell prompt

if [[ -d ~/thingy ]]
then
   source ~/thingy/init/init-thingy
fi

# Add additional directories to the PATH

export PATH=~/.local/bin:$PATH:~/thingy

# Set icons for apps that don't do it for thmselves

set-icons

# Try to run ssh-agent if it isn't already doing so

if [[ -z $(pgrep ssh-agent) ]]
then
   eval `ssh-agent -s`
fi

# Run 'keys' to run keychain before SSH to ensure that keys are loaded

alias keys='eval $(/usr/bin/keychain --eval --agents ssh -Q --quiet ~/.ssh/{*_rsa,bootstrap_*})'

eval $(SHELL=/bin/bash keychain --quiet --eval id_rsa)

ssh-add -q ~/.ssh/bootstrap* 2>/dev/null


# Chromaterm aliases

alias ifconfig='ifconfig "$@" | ct;'

# Prefix the prompt to remind me that I'm running in Bash

export PS1="*bash* $PS1"

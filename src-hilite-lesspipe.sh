#! /usr/bin/env bash

################################################################################
# Enhanced version of the script that comes with source-highlight - tries
# to determine the type of files without extensions.
################################################################################

for source in "$@"
do
   case $source in
      *ChangeLog|*changelog )
         source-highlight --failsafe -f esc --lang-def=changelog.lang --style-file=esc.style -i "$source"
         ;;

      *Makefile|*makefile )
         source-highlight --failsafe -f esc --lang-def=makefile.lang --style-file=esc.style -i "$source"
         ;;

      *.tar|*.tgz|*.gz|*.bz2|*.xz )
         lesspipe "$source"
         ;;

      *)
         filetype=$(file "$source")
         case "$filetype" in
            *Python* )
               lang="Python"
               ;;

            "*shell script*" )
               lang="sh"
               ;;

            * )
               lang=""
               ;;
         esac

         if [[ -z "$lang" ]]
         then
            source-highlight --failsafe --infer-lang -f esc --style-file=esc.style -i "$source"
         else
            source-highlight --failsafe --src-lang="$lang" -f esc --style-file=esc.style -i "$source"
         fi
         ;;
   esac
done

# Thingy

Licence: GPL v3

Author: John Skilleter v0.99

Collection of shell utilities and configuration stuff for Linux and, now, for MacOS.

Currently in a alpha/beta stage - usable, with a few rough edges, but incomplete.

It comprises Thingy Core, which is a set of Python and Shell scripts and Full-Fat Thingy which installs a large number of additional packages, git repos and configuration which
are generally specific to me and my ways of working.

# TODO: Split skilleter_thingy and the thingy setup stuff into separate Git repos - thingy setup should just treat skilleter_thingy as any other Python package

# Overview

## Installing Full-Fat Thingy on Ubuntu

To install Full-Fat Thingy on Ubuntu, clone the git repo into your home directory and run the installer. This will give you a setup that is very much customised to
my way of working, so you wish to use the instructions in the next section to install Thingy Core.

```
    bash
    cd ~
    git clone https://gitlab.com/skilleter/thingy
    cd thingy
    ./thingy-config
```
It does have a few requirements including Python v3 and m4 which must be installed first.

Installing Thingy this way uses the `install.ini` file to install and configure the system which will:

* Install a large number of packages (some of which are only installed on desktop systems and some are specific to certain Ubuntu releases)
* Configure additional PPAs for rvm, fish shell, Zim, kdenlive, get-iplayer, etc.
* Install a number of Python packages
* Install Snap packages for gimp, slack, etc.
* Install Flatpak pacakges for diffuse, chrome, etc.
* Clone a number of Git repos
* Install a number of crontab jobs

On other Linux distributions derived from Debian the installer stands a reasonable chance of working.

It will definitely not work on non-Debian distributions of Linux or on MacOS.

## Installing Thingy Core

To install Thingy-Core run the `thingy-core` script to install the Python scripts and copy the contents of the `scripts` directory to `~/.local/bin` - you may need to add
this directory to your path if your system does not do this automatically and, if your system does not automatically put Python packages on your path you may need
to do this as well; the `py-install` script will tell you to do this if necessary.

Installing this way will give you Thingy functionality without my personalised configuration and additional packages.

## Updating

To update Thingy, simply pull updates into the git working tree and re-run the installer (either `thingy-config` or `thingy-core`).

# Configuration

When Full-Fat Thingy is installed it sets up a number of configuration files for shell utilities:

* .bash_aliases (see the 'Aliases' section)
* .inputrc - configures the Bash command line editor
* .pylintrc - configures PyLint
* .xbindkeysrc - configures mouse buttons 4 and 5 as page up and page down keys
* .config/terminator/config - configures the Terminator terminal
* .gitconfig (updated by Thingy if it already exists)

Thingy expects that the user is using the Bash shell and that the .bashrc file that runs when the user logs in
runs the .bash_aliases script if it is present. The Thingy initialisation code is in this file and .bashrc should
be manually modified to source it if it does not do so by default.

In addition, if a file called .bashuser is present in the home directory, this will be run as part of Thingy initialisation
this allows the user to add their own configuration data without modifying Thingy.

Thingy will also work with other default shells, e.g. zsh and fish, but will require some manual setup.

Note that I have now moved to the Fish shell, so the Bash functionality in Full-Fat Thingy will be deprecated at some point in the future.

## Bash Configuration Settings

A number of configuration settings are set up in the .bash_aliases file - these are generally set my personal preferences, so
feel free to argue with me about these.

* TERM set to xterm-256color
* Default pager set to 'less'
* Less configured to use the source-highlighter package if it is installed
* Shell history configured to save up to 5000 entries with a maximum size of 20,000, duplicates to be ignored and history appended and history is synced across shell instances.
* Bash completion is enabled
* Extended globbing enabled in the shell
* The thingy directory is added to path
* The shell prompt is configured (see below)
* The keyboard layout and shell keyboard functionality are configured
* The console colours are configured
* Colour highlighting is configured for the ls command (and any other command that supports the .dircolors file)
* Authcomplete is configured.

## Shell Prompt

Full-Fat Thingy configures the shell prompt with a number of colour-coded entries:

* If connected via SSH, the hostname in white on black
* The shell nesting level in square brackets - shown in magenta normally or in black if running under screen or tmux
* If in a git working tree, the git status is shown where red indicates that there are locally-modified files, cyan that there are local files that are
either not ignored or have not been added to git, yellow indicates that there are local uncommitted changes and green that the working tree is clean. The status
indicates the name of the upstream remote (if one is configured) and the name of the local repo, the current branch (if any) and whether a rebase is in progress.
* The current directory, with intermediate directories shown as '...' if the current path is longer than 1/3 of the width of the terminal.

# Thingy Commands

## Aliases and shell functions (aka thinglets):

These are only available when running the Bash shell

### .., ..., ...., .....

Equivalent to 'cd ..', 'cd ../..', 'cd ../../..' and 'cd ../../../..'.

It is possible that the '......' command may be added in future.

### dir, dira, dirt, dirta

dir is equivalent to 'ls -lh'

dira is equivalent to 'ls -lah'

dirt is equivalent to 'ls -rlth'

dirta is equivalent to 'ls -ralth'

### grep, rgrep, egrep and fgrep

Override the default grep, rgrep, egrep and fgrep commands, adding options to ignore '.git' directories and enable colour highlighting.

### l, la, ll and ls

l is equivalent to 'ls -CF'

la is equivalent to 'ls -A'

ll is equivalent to 'ls -alF'

ls is equivalent to 'ls --group-directories-first --color=auto'

### more

Equivalent to 'less'

### update-dircolours

Re-evaluate the .dircolors file

### diffuse

If diffuse is installed as a flatpak, run it.

### dropstart

Start Dropbox if it isn't already running

### ftrace

Run ftrace tracking just file I/O.

### install-jed

Clone my version of the Jed editor, build and install the console and X versions then clone my configuration data.

### jsonview

Pretty-print a JSON file

### locatex

Searches for exact matches to a filespec and displays them as if output by 'ls -ld'.

### musgrep

Grep for data in Musescore score files

### pep

Run autopep8 either on specified files or on Python files in the current directory

### prunetime

Filter to remove strings that resembles times/dates.

### remotex

Start a new X server in a window then connect a remote client to it

### server-backup

My backup software using Borg Backup to backup to a Synology NAS

### set-app-icon

Set the icon for the current active window or for all windows matching a specified title.

### set-icons

Set the app icons for Slack and KSysGuard (if the icons are present) - fixes a bug that means XFCE shows blank icons.

### tag

Generate a tag file for the current directory using ctags

### thingy-config

Install script - Thingy is normally cloned into the user's home directory and the install script is used after the initial clone or subsequent pulls,
to install or update Thingy. See the 'Installation' section for details.

### thingy-lint

Run pylint on all the Thingy Python source

### vb-time-sync

Re-sync the guest clock when it gets out of sync with the host machine

### webmon

Monitor a web site and report status

### vpn

Run OpenVPN to connect to a VPN with a configuration file stored in ~/openvpn

## Thingy Git Commands and Aliases

Full-Fat Thingy configures a couple of hooks using the template directory feature of git init and clone (see the git help for these commands).
The template directory is 'git-templates' and it contains a 'hooks' subdirectory containing symlinks to the actual hook files in the git-templates directory.
The hooks are:

* post-commit - Warns the user about whitespace errors and TODO notes in the files being committed
* pre-push    - If remote tracking is not configured for the branch, it configures it.
* pre-commit  - Warns the user if they are committing to main, master or develop in a git-flow environment
* commit-msg  - Enforce the commit message format if the remote is the e-RS Gitlab

To add a new hook:

* Create the hook in the 'git-hooks' directory and chmod it a+x
* Create an absolute symlink to the hook in the 'git-templates/hooks' directory

Thingy has a number of git-related commands, many of which can be invoked as git subcommands (e.g. `git review`)

### cdg (alias)

Change directory to the top-level directory of the current Git working tree.

### gits (alias)

Shorthand for 'git status'

### git branch-history

List the most recent commit date on each local branch.

### git cmp

Improved version of 'git diff' and 'git difftool' - reports statistics about the changes
to each file and uses a graphical diff tool by default.

### git rc (alias for 'git rebase --continue')

Continue a rebase operation

### git ra (alias for 'git rebase --abort')

Abort the current rebase operation

### git ri (alias for 'git rebase -i')

Start an interactive rebase

### git cp (alias for 'git cherry-pick')

Perform a cherry-pick

### git unstage (alias for 'git reset HEAD --')

Unstage changes

### git ws (alias)

Remove unnecessary whitespace changes from the most recent commit

### git br (alias for 'git branch -vv')

Verbosely list branches

### git hold

Archive, list or de-archive branches.

A branch is archived by tagging it and deleting the branch and de-archive by re-creating the branch from the tag and deleting the tag.

## Git Configuration

Full-Fat Thingy sets up the .gitconfig file in the user's home directory which is used to store configuration setting for the Thingy Git commands, this can be edited by the user (using a text editor) to customise the behaviour of these commands.

The configuration items currently used are described below, along with their default and valid values. The default values are set by the Full-Fat Thingy installer.

### core.editor (default: run_jed_f)

The name of the text editor used to edit commit messages.

### core.page (default: cat)

Can be set to a utility such as less or more to paginate output from Git commands

### color.ui (default: auto)

Use colour highlighting in output from Git commands when they output to the console.

### git.renames (default: true)

TODO: details for git.renames

### diff.tool (default: diffuse)

The graphical diff tool used by Git commands

### push.default

TODO: Details for push.default

### merge.tool (default: meld)

The graphical merge tool used by Git commands

### pager.log (default: less)

TODO: Details for pager.log

### cmp.debug (default: false)

Set to true to enable debug output from the 'git cmp' command.

### rerere.enabled (default: true)

Enable the git rerere command - see https://git-scm.com/docs/git-rerere

### init.templatedir (default: ~/thingy/git-templates)

Specifies the directory from which templates are copied when a new working tree is created.

The templates supplied with Thingy just consist a post commit hook that resolves any
whitespace issues (e.g. mixing of tabs and whitespace, or trailing whitespace) with the commit automatically.

### core.whitespace (default: trailing-space,space-before-tab)

Specifies the types of whitespace issues that Git will detect and complain about. Options are:

blank-at-eol which looks for spaces at the end of a line
blank-at-eof, which notices blank lines at the end of a file
space-before-tab, which looks for spaces before tabs at the beginning of a line.
indent-with-non-tab, which looks for lines that begin with spaces instead of tabs
tab-in-indent, which watches for tabs in the indentation portion of a line
cr-at-eol, which tells Git that carriage returns at the end of lines are OK.

### apply.whitespace (default: fix)

Specify the default --whitespace option used by the git patch command.

### prompt.prefix (default: 2)

The level of detail shown for the current git status in the prompt when the current directory is inside a Git working tree.

Valid values are:

* 0 - No status shown
* 1 - Abbreviated status
* 2 - Verbose status

## Python Modules

Thingy also contains a number of Python v3 modules used by thingies.

## aws.py

AWS-related functions

## colour.py

Colour handling

## files

Additional file-handling functions

## git & git2

Git functions

## gitlab

GitLab functions

## logger

Logging functions (obsolete and based on Python's logging module)

## path

Path-handling functions

## popup

Pop-up windows for curses module

## process & run

Process-handling functions

## tidy

Functions for cleaning text

## pane

Curses pane-handling functionality, used by tfm

# Bugs and Possible Future Features

## BUGS:

* Commit hook fails on the first commit to a repo
* Installer should check that prequisites are installed when it runs, rather than failing.

## Possible Git Commands:

* git addignore - add .gitignore files to empty directories
* git branchdiff - list commits that are unique on one branch compared to another
* git cmp improvements
 handle renames
 symlinks
 binary files
 viewing new files
 show log summary between diff points.
* git import
 Import a directory tree into git - check local tree is clean, copy source directory in, add .gitignores in empty directories, warn about DOS line endings, .git* files/directories in source, warn about filenames with spaces
* git local - list local files in a tree
* git log - with branch tree
* git mt - shorthand for 'git mergetool'
* git tags - list tagged revisions

## Other possible commands:

* addspell - add words to ispell dictionary
* ansiclean - clean ANSI control sequences from file/pipe
* log file filter - remove ANSI sequences, time, date, hostnames, etc.
* build log analyser - extract details of errors and warnings
* build log highlighting
* ccache monitor
* gi - graphical info pages using Konqueror
* gman - list man page in konqueror
* grep using Python re's
* hex - hexdump with ascii
* mountinfo - display mount information including filesystem info
* pathmod - add/remove entries from $PATH-type string
* prompt - set shell level colour according to SSH/tmux/screen use
* run ddd with command line passed to executable being debugged
* tag - build ctags and cscope files
* tool for monitoring open files
* wrapper for GNU make with output highlighting

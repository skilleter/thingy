FROM ubuntu:22.04

ARG BUILD_USER=build
ARG BUILD_UID=1000
ARG BUILD_GID=1000
ARG HOME_DIR=/home/$BUILD_USER

ENV DEBIAN_FRONTEND noninteractive
ENV TERM xterm-256color

# Update and upgrade

RUN apt-get update --yes --fix-missing
RUN apt-get upgrade --yes

# Install essentials

RUN apt-get install --yes python3 curl python3.10-venv sudo

RUN useradd -l -u ${BUILD_UID} ${BUILD_USER} && \
    mkdir -p ${HOME_DIR} && \
    chown -R ${BUILD_UID}:${BUILD_GID} ${HOME_DIR}

COPY . ${HOME_DIR}/thingy
RUN chown -R ${BUILD_USER} ${HOME_DIR}/thingy

USER ${BUILD_UID}

WORKDIR ${HOME_DIR}

RUN curl https://bootstrap.pypa.io/get-pip.py -o /tmp/get-pip.py && python3 /tmp/get-pip.py
RUN python3 -m pip install pipx
ENV PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:${HOME_DIR}/.local/bin
RUN pipx ensurepath

# Copy Thingy into place

# Run the CI test code in a secondary shell so it picks up the Thingy config

CMD echo -e "********************************************************************************\n* Running CI code\n********************************************************************************"

CMD bash -c 'cd /root/thingy && ./thingy-config'
